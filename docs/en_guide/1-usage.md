# Introduction

exportDBmapper is a QGIS 3 plugin to map export between PostgreSQL database and GPKG file. It consists of a mapping editor and processings in the toolbox.

## Mapping editor

The mapping editor will allow you to define your export between PG database and GPKG file. There is two tabs in the mapping editor dialog. The first one is used to map export from PG database to GPKG file. The second is used to  export GPKG to PG database. Each process will produce a YAML file with your parameter. It is possible to import the YAML file in the dialog to modify an existing export configuration.

![mapping_editor1](/_static/images/mapping_editor1.png)

*PG to GPKG mapper*

*Append to an existing GPKG* option will allow you to add table to an existing GPKG file instead of erasing the initial GPKG file.

![mapping_editor2](/_static/images/mapping_editor2.png)

*GPKG to PG mapper*


## Export processing

There is two processing in the toolbox.

![processing_toolbox](/_static/images/processing_toolbox.png)

Each processing take a YAML file as input, and process the export.
