# {{ title }} - Documentation

> **Author and contributors:** {{ author }}
>
> **Version:** {{ version }}
>
> **Source code:** {{ repo_url }}

```{toctree}
---
caption: English user guide
maxdepth: 1
glob:
---
en_guide/*
```


```{toctree}
---
caption: Guide d'utilisation en Français
maxdepth: 1
glob:
---
fr_guide/*
```

----

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
development/contribute
development/environment
development/documentation
development/packaging
history
```
