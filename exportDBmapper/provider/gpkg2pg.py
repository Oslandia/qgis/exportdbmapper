# -----------------------------------------------------------------
# Copyright (C) 2021 Oslandia
# Conctact : sebastien.peillet@oslandia.com ; contact@oslandia.com
# -----------------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os

import yaml
from qgis import processing
from qgis.core import (
    QgsDataSourceUri,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterFile,
    QgsProcessingParameterFileDestination,
    QgsProject,
    QgsVectorLayer,
)
from qgis.PyQt.QtCore import QCoreApplication


class ExportGPKGToPGAlgorithm(QgsProcessingAlgorithm):
    """
    ExportPGToGPKGAlgorithm is an algorithm that takes a yaml file
    created by the plugin to do batch export between PG database and GPKG file.
    """

    INPUT = "INPUT"
    TRUNC = "TRUNC"
    PROJECT = "PROJECT"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate(self.__class__.__name__, string)

    def createInstance(self):
        return ExportGPKGToPGAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Exportgpkg2pg"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Export gpkg to pg")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Tools")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "tools"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "Exportgpkg2pg is an algorithm that takes a yaml file created by the plugin to do batch export between GPKG fileand PG database."
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT, self.tr("Input yaml"), extension="yaml"
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(
                self.TRUNC, self.tr("Truncate table"), defaultValue=False
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.PROJECT,
                self.tr("Project output"),
                fileFilter="*.qgs",
                optional=True,
                defaultValue="",
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Execute pg2gpkg export as batch conversion
        """

        source = self.parameterAsFile(parameters, self.INPUT, context)
        truncate = self.parameterAsBoolean(parameters, self.TRUNC, context)
        output_project = self.parameterAsFileOutput(parameters, self.PROJECT, context)

        with open(source, "r") as yamlfile:
            data = yaml.load(yamlfile, Loader=yaml.FullLoader)

        outputs = dict()
        for d in data.values():
            gpkg_layer = QgsVectorLayer(
                d["gpkg_file"] + "|layername=" + d["gpkg_table"], d["gpkg_table"], "ogr"
            )
            pk = gpkg_layer.fields()[gpkg_layer.primaryKeyAttributes()[0]].name()
            if truncate:
                gpkg_layer.setSubsetString("{} is None".format(pk))

            param = {
                "INPUT": gpkg_layer,
                "DATABASE": d["connection_name"],
                "SCHEMA": d["db_schema"],
                "TABLENAME": d["db_table"],
                "PRIMARY_KEY": gpkg_layer.fields()[
                    gpkg_layer.primaryKeyAttributes()[0]
                ].name(),
                "GEOMETRY_COLUMN": "geom",
                "ENCODING": "UTF-8",
                "OVERWRITE": True,
                "CREATEINDEX": True,
                "LOWERCASE_NAMES": True,
                "DROP_STRING_LENGTH": False,
                "FORCE_SINGLEPART": False,
            }
            processing.run("qgis:importintopostgis", param)
            uri = QgsDataSourceUri(d["db_conn"])
            uri.setDataSource(d["db_schema"], d["db_table"], "geom")
            if d["connection_name"] in outputs:
                outputs[d["connection_name"]].append(uri)
            else:
                outputs[d["connection_name"]] = [uri]

            feedback.pushInfo(
                d["gpkg_file"]
                + "|layername="
                + d["gpkg_table"]
                + self.tr(" has been exported to ")
                + d["connection_name"]
                + ":"
                + d["db_schema"]
                + "."
                + d["db_table"]
            )

        if output_project:
            project = QgsProject()

            def add_layer(source, name, provider, group=None):
                """Add a layer to the QGIS project, with respect to some connection parameters.

                Parameters
                ----------
                source : str
                    Layer source.
                name : str
                    Layer display name in the layer list
                provider : str
                    Layer provider.
                group : str
                    If None the layer is set a root layer, otherwise the layer is stored into the
                given group

                Returns
                -------
                QgsVectorLayer
                    Layer that will be added to the QGIS project.
                """
                layer = QgsVectorLayer(source, name, provider)
                project.addMapLayer(layer, addToLegend=group is None)
                if group is not None:
                    group.addLayer(layer)
                return layer

            root_group = project.layerTreeRoot()
            for f, layers in outputs.items():
                group_name = os.path.splitext(os.path.basename(f))[0]
                group = root_group.addGroup(group_name)
                for l in layers:
                    add_layer(l.uri(), uri.table(), "postgres", group)

            project.write(output_project)
        return {}
