# -----------------------------------------------------------------
# Copyright (C) 2021 Oslandia
# Conctact : sebastien.peillet@oslandia.com ; contact@oslandia.com
# -----------------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os

from qgis.core import QgsProcessingProvider
from PyQt5.QtGui import QIcon

from .pg2gpkg import ExportPGToGPKGAlgorithm
from .gpkg2pg import ExportGPKGToPGAlgorithm

# from .datagen import DatagenQDeepLandiaProcessingAlgorithm


class ExportDBMapperProvider(QgsProcessingProvider):
    def loadAlgorithms(self, *args, **kwargs):
        pass
        self.addAlgorithm(ExportPGToGPKGAlgorithm())
        self.addAlgorithm(ExportGPKGToPGAlgorithm())

    def id(self, *args, **kwargs):
        """The ID of your plugin, used for identifying the provider.

        This string should be a unique, short, character only string,
        eg "qgis" or "gdal". This string should not be localised.
        """
        return "ExportDBMapper"

    def name(self, *args, **kwargs):
        """The human friendly name of your plugin in Processing.

        This string should be as short as possible (e.g. "Lastools", not
        "Lastools version 1.0.1 64-bit") and localised.
        """
        return self.tr("ExportDBMapper")

    def icon(self):
        """Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QIcon(os.path.join(os.path.dirname(__file__),'..','icon.png'))

