# -----------------------------------------------------------------
# Copyright (C) 2021 Oslandia
# Conctact : sebastien.peillet@oslandia.com ; contact@oslandia.com
# -----------------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os

import yaml
from qgis import processing
from qgis.core import (
    QgsDataSourceUri,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterFile,
    QgsProcessingParameterFileDestination,
    QgsProject,
    QgsVectorLayer,
)
from qgis.PyQt.QtCore import QCoreApplication


class ExportPGToGPKGAlgorithm(QgsProcessingAlgorithm):
    """
    ExportPGToGPKGAlgorithm is an algorithm that takes a yaml file
    created by the plugin to do batch export between PG database and GPKG file.
    """

    INPUT = "INPUT"
    TRUNC = "TRUNC"
    PROJECT = "PROJECT"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate(self.__class__.__name__, string)

    def createInstance(self):
        return ExportPGToGPKGAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Exportpg2gpkg"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Export pg to gpkg")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Tools")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "tools"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "Exportpg2gpkg is an algorithm that takes a yaml file created by the plugin to do batch export between PG database and GPKG file."
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT, self.tr("Input yaml"), extension="yaml"
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(
                self.TRUNC, self.tr("Truncate table"), defaultValue=False
            )
        )
        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.PROJECT,
                self.tr("Project output"),
                fileFilter="*.qgs",
                optional=True,
                defaultValue="",
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Execute pg2gpkg export as batch conversion
        """

        source = self.parameterAsFile(parameters, self.INPUT, context)
        truncate = self.parameterAsBoolean(parameters, self.TRUNC, context)
        output_project = self.parameterAsFileOutput(parameters, self.PROJECT, context)

        with open(source, "r") as yamlfile:
            data = yaml.load(yamlfile, Loader=yaml.FullLoader)

        outputs = dict()
        for d in data.values():
            uri = QgsDataSourceUri(d["db_conn"])
            uri.setDataSource(d["db_schema"], d["db_table"], "geom")
            pg_layer = QgsVectorLayer(uri.uri(), d["db_table"], "postgres")
            options = "-nln {} ".format(d["gpkg_table"])
            options += "-append " if d["append_mode"] else " "
            options += "-limit 0" if truncate else ""
            processing.run(
                "gdal:convertformat",
                {"INPUT": pg_layer, "OPTIONS": options, "OUTPUT": d["gpkg_file"]},
            )
            if d["gpkg_file"] in outputs:
                outputs[d["gpkg_file"]].append(d["gpkg_table"])
            else:
                outputs[d["gpkg_file"]] = [d["gpkg_table"]]
            feedback.pushInfo(
                d["db_schema"]
                + "."
                + d["db_table"]
                + self.tr(" has been exported to ")
                + d["gpkg_file"]
                + "|layername="
                + d["gpkg_table"]
            )

        if output_project:
            project = QgsProject()

            def add_layer(source, name, provider, group=None):
                """Add a layer to the QGIS project, with respect to some connection parameters.

                Parameters
                ----------
                source : str
                    Layer source.
                name : str
                    Layer display name in the layer list
                provider : str
                    Layer provider.
                group : str
                    If None the layer is set a root layer, otherwise the layer is stored into the
                given group

                Returns
                -------
                QgsVectorLayer
                    Layer that will be added to the QGIS project.
                """
                layer = QgsVectorLayer(source, name, provider)
                project.addMapLayer(layer, addToLegend=group is None)
                if group is not None:
                    group.addLayer(layer)
                return layer

            root_group = project.layerTreeRoot()
            for f, layers in outputs.items():
                group_name = os.path.splitext(os.path.basename(f))[0]
                group = root_group.addGroup(group_name)
                for l in layers:
                    add_layer(f + "|layername=" + l, l, "ogr", group)

            project.write(output_project)

        return {}
