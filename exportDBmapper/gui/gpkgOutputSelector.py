# -----------------------------------------------------------------
# Copyright (C) 2021 Oslandia
# Conctact : sebastien.peillet@oslandia.com ; contact@oslandia.com
# -----------------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os

from PyQt5.QtWidgets import QWidget, QLineEdit, QPushButton, QFileDialog, QHBoxLayout
from PyQt5 import uic


class GPKGOutputSelector(QWidget):
    def __init__(self, parent):
        super(GPKGOutputSelector, self).__init__()

        self.hLayout = QHBoxLayout(self)
        self.lineEdit = QLineEdit("")
        self.hLayout.addWidget(self.lineEdit)
        self.pushButton = QPushButton("...")
        self.hLayout.addWidget(self.pushButton)
        self.pushButton.clicked.connect(self.search)

    def search(self):
        file, _ = QFileDialog.getSaveFileName(self, self.tr("GPKG output"), filter="GPKG (*.gpkg)")
        if file:
            self.lineEdit.setText(file)

    def text(self):
        return self.lineEdit.text()

    def setText(self, text):
        self.lineEdit.setText(text)
