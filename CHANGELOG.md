# Changelog

## Unreleased

### Added

- set new row widget values from the last row values

### Modified

### Fixed

- during import connectionChanged and schemaChanged has to be emited to update other combobox

## 0.2.0 - 2021/03/31

### Added

- enable hook

### Modified

- exportDBmapper menu was moved into database menu
- GPKG -> PG and PG -> GPKG yaml converge and can be used in both processing

### Fixed

- some fixes in documentation


## 0.1.0 - 2021/03/29

- First version. PG database table can be exported to Geopackage and vice versa. The plugin use YAML file to map table between both format.
